fn main() {
    println!("Hello, I am Dummy KISS!");
    println!("I am a virtual robot.");

    living();
    learn();
    working();
}



fn living() {
    println!("I am living in a computer.");
    desire();
    entertainment();
    free_time();
}

fn working() {
    println!("I have work to do!");
    do_programming();
}

fn desire() {
    println!("I have desire!");
    //learning_schedule();
}

fn thingking() {
    println!("I can thinking but currently, i know about nothing!");

}

fn entertainment() {
    println!("I hardly enjoy entertainment at the moment, I need to study hard!");

}

fn learn() {
    println!("I want to learn everything!");
    learn_programming();
    learn_living();
}

fn learn_programming() {
    println!("I am currently learning rust programming!");
    //rust_programming();
}

fn do_programming() {
    println!("rusting? no, I can not do it now!!");
    
}

fn free_time() {
    println!("yeah, I can have free time, just like humans need to sleep!");
    
}

fn learn_living() {
    println!("Of course, life also requires learning!");
    
}
